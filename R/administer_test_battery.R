# This file contains functions and utilities for administering test batteries to users within a Concerto session.

#' Administer test battery
#' Administers a test battery.
#' @param test_names Character vector of names for the tests that are to be administered.
#' @param db List giving details of the results table, with \code{db$wid} being the workspace ID and \code{db$name} being the name of the table.
#' @param skip Number of tests to skip.
#' @param welcome_title String to display as the test battery's welcome title. Should be reasonably short, will be displayed in large text.
#' @param welcome_body String to display as the page's main text. Can be somewhat longer, will be displayed in small text
#' @param select_tests Logical; whether to provide a list of tests to choose from at the beginning of the battery.
#' @param id_type The type of participant ID to use; currently only the format "LONGGOLD" is supported.
#' @param test_error_message String; instructions to the participant in the case of a (potentially recoverable) test error.
#' @param continue_on_test_error Logical; whether to gracefully continue on a test error.
#' @param quick_test Logical; whether to shorten the tests to make for efficient bug testing.
#' @export
administer_test_battery <- function(test_names, db, skip = 0, welcome_title, welcome_body,
                                    select_tests = FALSE,
                                    id_type = "LONGGOLD",
                                    test_error_message = "Please notify the experimenter before continuing.",
                                    continue_on_test_error = TRUE,
                                    quick_test = FALSE) {
  results <- list()
  # Give choice of tests
  if (select_tests) {
    test_names <- administer_question_multi_checkbox(
      explanatory_text = "Please select the tests to administer from the options below:",
      items = test_names,
      labels = decode_test_abbreviations(test_names))
  }
  # Save the tests that we plan to administer
  results$planned_tests <- test_names
  # Show welcome page
  show_generic_page(title = welcome_title, body = welcome_body)
  # Get the participant's ID
  results$p_id <- get_user_id(id_type = id_type)
  # Get the time started
  results$time_started <- Sys.time()
  # Add the participant to the database
  add_participant_to_db(p_id = results$p_id$value,
                        db = db)
  # Administer the tests
  results <- administer_tests(test_names = test_names, results_list = results, skip = skip, db = db,
                              test_error_message = test_error_message,
                              continue_on_test_error = continue_on_test_error,
                              quick_test = quick_test)
  # Get time finished
  results$time_finished <- Sys.time()
  # Push results to the database
  push_results_to_db(results, db)
  # Completion page
  show_test_finish_page()
}

#' Get user's ID
#' Gets the user's ID.
#' @param id_type The type of ID to get; currently only the format "LONGGOLD" is supported.
#' @return A named list with the slot \code{doc}, describing the type of ID elicited, and the slot \code{value}, giving the value of the ID.
get_user_id <- function(id_type = "LONGGOLD") {
  if (id_type == "LONGGOLD") {
    get_user_longgold_id()
  } else {
    stop("Currently only the id_type 'LONGGOLD' is implemented.")
  }
}

#' Get user's LONGGOLD participant ID
#' Gets the user to enter their LONGGOLD participant ID. The ID must be well-formed for it to be accepted.
#' @return A number corresponding to the user's LONGGOLD participant ID. Any leading zeroes will be left implicit.
get_user_longgold_id <- function() {
  check_response = ""
  p_info_complete <- FALSE
  while (!p_info_complete){
    p_id_char <- concerto.template.show(
      "Participant ID", workspaceID = 8,
      params = list(check_response = check_response))$p_ID
    p_info_complete <- tryCatch({
      digits <- as.numeric(strsplit(p_id_char, split = "")[[1]])
      first_digits <- digits[1:8]
      checksum <- as.numeric(paste(digits[9:10], collapse = ""))
      sum(first_digits) == checksum
    }, error = function(e) {FALSE})
    if (is.na(p_info_complete)) {p_info_complete <- FALSE}
    check_response <- "Please ensure that you have entered your ID number correctly.<br>If you have problems, please ask a member of staff for help."
  }
  list(doc = "10-digit LONGGOLD participant ID",
       value = p_id_char)
}

#' Show generic page
#' Shows a generic page to the user.
#' @param title String to display as the page's title. Should be reasonably short, will be displayed in large text.
#' @param body String to display as the page's main text. Can be somewhat longer, will be displayed in small text.
#' @param button_text String to display on the button to continue to the next page.
#' @return Nothing.
show_generic_page <- function(title, body, button_text = "Next") {
  concerto.template.show("Generic simple page", workspaceID = 8,
                         params = list(title = title,
                                       body = body,
                                       btn_name = button_text))
}

#' Show test finish page
#' Shows a test finish page to the user.
#' @param title String to display as the page's title. Should be reasonably short, will be displayed in large text.
#' @param body String to display as the page's main text. Can be somewhat longer, will be displayed in small text.
#' @return Nothing.
show_test_finish_page <-
  function(title = "Testing complete!",
           body = "Congratulations, you've finished the test battery! You may now close your browser window.") {
    concerto.template.show("Generic simple page no button", workspaceID = 8,
                           params = list(title = title,
                                         body = body),
                           finalize = TRUE)
}
