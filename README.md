# README #

### What is this repository for? ###

This repository comprises an R package called *GMSIConcerto*, which contains utility functions for delivering tests related to the Goldsmiths Musical Sophistication Index (Gold-MSI) project. This package is intended both for deployment on Concerto servers and for analysing data from Concerto test sessions.

### How do I get set up? ###

* Open an R session
* Make sure you have the devtools package installed (you can install it by running `install.packages("devtools")`)
* Load the devtools package: `library(devtools)`
* You can then install the GMSIConcerto package as follows:
  * Run `install_bitbucket("goldmsi/gmsiconcerto")` if you want the *master* branch,
  * or run `install_bitbucket("goldmsi/gmsiconcerto", ref = "dev")` if you want the *development* branch.

### How do I update the Concerto server to use the latest code?

It's pretty much the same as on your local computer.

* SSH in
* Start R: `sudo R`
* Load devtools: `library(devtools)`
* Run `install_bitbucket("goldmsi/gmsiconcerto")` if you want the *master* branch, or run `install_bitbucket("goldmsi/gmsiconcerto", ref = "dev")` if you want the *development* branch.

### Contribution guidelines ###

* Please make sure that all exported functions are documented using the Roxygen syntax (see code for examples).
* Code may refer to Concerto platform functions, such as concerto.template.show(). The `concerto` package is not, however, added to the list of package dependencies, because it's inconvenient for developers to install this package on their local machines.
* The *master* branch must always be production-ready; only merge tested code to *master*. New features should be developed by creating a feature branch off the *dev* branch, then merging that feature branch back onto *dev* when the feature is completed. Use *pull requests* to notify the rest of the team about your proposed changes. For more detail about the workflow, please see the following link: http://nvie.com/posts/a-successful-git-branching-model/

### Who do I talk to? ###

If you have any questions, get in touch with Peter at p.m.c.harrison@qmul.ac.uk.